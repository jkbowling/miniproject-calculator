package server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Arrays;

import services.CalculatorInterface;
import services.Calculator;

public class Server {
    public static void main(String args[]) {
        int port  = 1099;
        try {
            CalculatorInterface skeleton = (CalculatorInterface)UnicastRemoteObject.exportObject(new Calculator(), 0);
            Registry registry = LocateRegistry.getRegistry(port);
            registry.rebind("Calculator", skeleton);
            System.out.println("Service Calculator lié au registre");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
