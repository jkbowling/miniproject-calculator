package client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

import services.CalculatorInterface;
import client.gui.CalculatorFrame;

public class Client {
    public static void main(String args[]) {
        String machine = "localhost";
        int port = 1099;
        try {
            Registry registry = LocateRegistry.getRegistry(machine, port);
            CalculatorInterface obj = (CalculatorInterface)registry.lookup("Calculator");
            CalculatorFrame calculatorFrame = new CalculatorFrame(obj);
        } catch (Exception e) {
            System.out.println("Client exception: " +e);
        }
    }
}
