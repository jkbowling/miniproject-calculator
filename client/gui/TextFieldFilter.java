package client.gui;

import javax.swing.text.DocumentFilter;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.JButton;


class TextFieldFilter extends DocumentFilter {
    private CalculatorFrame calculator;

    public CalculatorFrame getCalculator() {
        return this.calculator;
    }

    public TextFieldFilter(CalculatorFrame calculator) {
        super();
        this.calculator = calculator;
    }

    @Override
    public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
        Document doc = fb.getDocument();
        StringBuilder sb = new StringBuilder();
        sb.append(doc.getText(0, doc.getLength()));
        sb.insert(offset, string);

        if (isValidInput(string)) {
            if (!isOperator(string)) {
                super.insertString(fb, offset, string, attr);
            }
            else {
                if (!hasOperator()) {
                    super.insertString(fb, offset, " " + string + " ", attr);
                    for (JButton operatorButton : calculator.getOperatorButtons()) {
                        operatorButton.setEnabled(false);
                    }
                }
            }
        } else {
            // warn the user and don't allow the insert
        }
    }

    private boolean hasOperator() {
        // if operator buttons are enabled, then there is no operator
        return !this.getCalculator().getOperatorButtons()[0].isEnabled();
    }

    private boolean isOperator(String text) {
        return (text.equals("+") || text.equals("-") || text.equals("*") || text.equals("/"));
    }

    private boolean isValidInput(String text) {
        // if the input is a number or an operator
        return (text.matches("\\d+") || isOperator(text));
    }

    @Override
    public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {

        Document doc = fb.getDocument();
        StringBuilder sb = new StringBuilder();
        sb.append(doc.getText(0, doc.getLength()));
        sb.replace(offset, offset + length, text);

        // check the last character of the input
        String lastChar = text.substring(text.length() - 1);
        if (isValidInput(lastChar)) {
            if (!isOperator(lastChar)) {
                super.replace(fb, offset, length, text, attrs);
            }
            else {
                if (!hasOperator()) {
                    String textWithoutLast = text.substring(0, text.length() - 1);
                    super.replace(fb, offset, length, textWithoutLast + " " + lastChar + " ", attrs);
                    for (JButton operatorButton : calculator.getOperatorButtons()) {
                        operatorButton.setEnabled(false);
                    }
                }
            }
        } else {
            System.out.println("input invalide avec " + text);
            // warn the user and don't allow the insert
        }

    }

    @Override
    public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
        Document doc = fb.getDocument();
        StringBuilder sb = new StringBuilder();
        sb.append(doc.getText(0, doc.getLength()));

        String deletedText = sb.substring(offset, offset + length);
        int nbToErase = adjustErase(deletedText, sb, offset);

        sb.delete(offset, offset + length);

        if (nbToErase != 0) {
            if (nbToErase < 0) {
                super.remove(fb, offset + nbToErase, length - nbToErase);
            } else {
                if (nbToErase == 3)
                    super.remove(fb, offset - 1, length + 2);
                else
                    super.remove(fb, offset, length + nbToErase);
            }
        } else {
            super.remove(fb, offset, length);
        }
    }

    public int adjustErase(String deletedText, StringBuilder sb, int offset) {
        // give the number of characters to erase in the remaining text (negative for the left and positive for the right)
        int indexSpace = deletedText.indexOf(' ');
        boolean recoverButtons = (indexSpace >= 0) || (isOperator(deletedText.charAt(0)));
        int numberToErase = 0;
        if (indexSpace == -1) {
            if (isOperator(deletedText.charAt(0))) {
                numberToErase = 3;
            }
        } else if (indexSpace == 0) {
            if (deletedText.length() == 1) {
                if (isOperator(sb.toString().charAt(offset + 1))) {
                    numberToErase = 2;
                } else {
                    numberToErase = -2;
                }
            } else {
                if (isOperator(deletedText.charAt(1))) {
                    numberToErase = 1;
                } else {
                    numberToErase = -2;
                }
            }
        } else if (indexSpace == 1) {
            if (isOperator(deletedText.charAt(0))) {
                // the operator is just after the deleted text
                numberToErase = -1;
            }
            else {
                if (deletedText.length() == 2)
                    numberToErase = 2;
                else if (deletedText.length() == 3)
                    numberToErase = 1;
                else
                    numberToErase = 0; // the case 1 has already been treated
            }
        } else if (indexSpace == deletedText.length() - 1) {
            // as the case 0 has already been tested, deletedText has at least 2 char
            if (! isOperator(deletedText.charAt(deletedText.length() - 2))) {
                // the operator is just after the deleted text
                numberToErase = 2;
            }
            else {
                numberToErase = 0; // the case 1 has already been treated
            }
        } else if (indexSpace == deletedText.length() - 2) {
            if (isOperator(deletedText.charAt(deletedText.length() - 1))) {
                // the operator is just after the deleted text
                numberToErase = 1;
            }
            else {
                numberToErase = 0; // the case 0 or 1 has already been treated
            }
        } else {
            // int i = 0;
            // for (int i = 0 ; i < deletedText.length() ; i++) {
            //     recoverButtons = (! recoverButtons && ! isOperator(deletedText.charAt(i)));
            // }
        }

        if (recoverButtons) {
            for (JButton operatorButton : calculator.getOperatorButtons()) {
                operatorButton.setEnabled(true);
            }
        }

        return numberToErase;
    }

    public boolean isOperator(char letter) {
        return ("+-*/".indexOf(letter) >= 0);
    }

}
