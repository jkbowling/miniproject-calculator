package client.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.text.PlainDocument;
import javax.swing.JOptionPane;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import services.CalculatorInterface;


public class CalculatorFrame extends JFrame {

    private JTextField screen = new JTextField("0");
    private JPanel container = new JPanel();
    private JButton[] operatorButtons = {new JButton("+"), new JButton("-"), new JButton("*"), new JButton("/")};
    private JButton clearButton;
    private CalculatorInterface calculator;

    public JTextField getScreen() {
        return this.screen;
    }

    public JPanel getContainer() {
        return this.container;
    }

    public JButton[] getOperatorButtons() {
        return this.operatorButtons;
    }

    public CalculatorInterface getCalculator() {
        return this.calculator;
    }

    public CalculatorFrame(){
        super("Calculatrice");
        this.setLocationRelativeTo(null);
        this.setPreferredSize(new Dimension(500, 500));
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.initMainPanel();
        this.setContentPane(this.container);
        this.pack();
        this.setVisible(true);
    }

    public CalculatorFrame(CalculatorInterface calculator){
        super("Calculatrice");
        this.calculator = calculator;
        this.setLocationRelativeTo(null);
        this.setPreferredSize(new Dimension(500, 500));
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.initMainPanel();
        this.setContentPane(this.container);
        this.pack();
        this.setVisible(true);
    }

    private void initMainPanel() {
        PlainDocument doc = (PlainDocument) screen.getDocument();
        doc.setDocumentFilter(new TextFieldFilter(this));

        this.getScreen().setFont(new Font("Arial", Font.BOLD, 25));
        this.getScreen().setPreferredSize(new Dimension(500, 50));
        this.getScreen().addActionListener(new EqualListener(this));
        BorderLayout bLayout = new BorderLayout();
        bLayout.setHgap(5);
        bLayout.setVgap(5);
        this.container.setLayout(bLayout);
        this.container.add(this.screen, BorderLayout.NORTH);
        JPanel numberPanel = new JPanel();
        GridLayout gLayout = new GridLayout(4, 3);
        gLayout.setHgap(5);
        gLayout.setVgap(5);

        Font font = new Font("Arial", Font.BOLD, 20);
        numberPanel.setLayout(gLayout);
        for (int i = 1 ; i <= 9 ; i++) {
            JButton numberButton = new JButton(Integer.toString(i));
            numberButton.addActionListener(new NumberListener());
            numberButton.setFont(font);
            numberPanel.add(numberButton);
        }
        JButton zeroButton = new JButton("0");
        zeroButton.addActionListener(new NumberListener());
        zeroButton.setFont(font);
        numberPanel.add(zeroButton);
        JButton equalButton = new JButton("=");
        equalButton.addActionListener(new EqualListener(this));
        equalButton.setFont(font);
        numberPanel.add(equalButton);
        this.container.add(numberPanel, BorderLayout.CENTER);

        JPanel operatorPanel = new JPanel();
        operatorPanel.setPreferredSize(new Dimension(100, 450));
        gLayout = new GridLayout(6, 1);
        gLayout.setHgap(5);
        gLayout.setVgap(5);
        operatorPanel.setLayout(gLayout);

        JButton clearEButton = new JButton("CE");
        clearEButton.addActionListener(new ClearEListener());
        clearEButton.setFont(font);
        operatorPanel.add(clearEButton);
        clearButton = new JButton("C");
        clearButton.addActionListener(new ClearListener());
        clearButton.setFont(font);
        operatorPanel.add(clearButton);
        for (JButton operatorButton : this.operatorButtons) {
            operatorButton.addActionListener(new OperatorListener());
            operatorButton.setFont(font);
            operatorPanel.add(operatorButton);
        }
        this.container.add(operatorPanel, BorderLayout.EAST);
    }

    public void displayException(String title, Exception exception) {
        JOptionPane.showMessageDialog(this,
                exception.getClass().getName() + " : " + exception.getMessage() + "\n",
                title,
                JOptionPane.WARNING_MESSAGE);
        clearButton.doClick();
    }

    public static void main(String[] args) {
        CalculatorFrame calculatorFrame = new CalculatorFrame();
    }



    class NumberListener implements ActionListener {

        public void actionPerformed(ActionEvent actionEvent) {
            if (getScreen().getText().equals("0")) {
                getScreen().setText(actionEvent.getActionCommand());
            } else {
                getScreen().setText(getScreen().getText() + actionEvent.getActionCommand());
            }
        }

    }


    class OperatorListener implements ActionListener {

        public void actionPerformed(ActionEvent actionEvent) {
            getScreen().setText(getScreen().getText() + actionEvent.getActionCommand());
            for (JButton operatorButton : getOperatorButtons()) {
                operatorButton.setEnabled(false);
            }
        }

    }

    class ClearEListener implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent) {
            String screenContent = getScreen().getText().replaceAll("\\s+$", "");
            if (screenContent.length() <= 1) {
                getScreen().setText("0");
            } else if (screenContent.length() > 1){
                char lastChar = screenContent.charAt(screenContent.length() - 1);
                screenContent = screenContent.substring(0, screenContent.length() - 1);
                screenContent = screenContent.replaceAll("\\s+$", "");
                if (screenContent.length() == 0) {
                    getScreen().setText("0");
                }
                getScreen().setText(screenContent);
                ArrayList<Character> operators = new ArrayList<Character>();
                operators.add('+');
                operators.add('-');
                operators.add('*');
                operators.add('/');
                if (operators.contains(lastChar)) {
                    for (JButton operatorButton : getOperatorButtons()) {
                        operatorButton.setEnabled(true);
                    }
                }
            }
        }
    }

    class ClearListener implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent) {
            getScreen().setText("0");
            for (JButton operatorButton : getOperatorButtons()) {
                operatorButton.setEnabled(true);
            }
        }
    }

    class EqualListener implements ActionListener {
        private CalculatorFrame frame;

        public EqualListener(CalculatorFrame frame) {
            this.frame = frame;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            try {
                getScreen().setText(String.valueOf(getCalculator().compute(getScreen().getText())));
                for (JButton operatorButton : getOperatorButtons()) {
                    operatorButton.setEnabled(true);
                }
            } catch (Exception e) {
                frame.displayException("Une erreur est survenue", e);
            }
        }
    }
}
