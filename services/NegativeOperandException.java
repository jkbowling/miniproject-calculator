package services;

public class NegativeOperandException extends Exception {
  public NegativeOperandException(){
    super("Un opérande est négatif");
  }
}
