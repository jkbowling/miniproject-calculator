package services;

public class Calculator implements CalculatorInterface {

    public int compute(String operation) throws Exception {
        String[] parsedOperation = operation.split(" ");
        if (parsedOperation.length != 3) {
            throw new InvalidSyntaxException();
        } else {
            try {
                int int1 = Integer.valueOf(parsedOperation[0]);
                int int2 = Integer.valueOf(parsedOperation[2]);

                String operator = parsedOperation[1];
                if (int1 >= 0 && int2 >= 0) {
                    switch(operator) {
                        case "+" :
                            return int1 + int2;
                        case "-" :
                            if (int1 - int2 < 0) {
                                throw new NegativeResultException();
                            } else {
                                return int1 - int2;
                            }
                        case "*" :
                            return int1 * int2;
                        case "/" :
                            if (int2 == 0) {
                                throw new ZeroDivisionException();
                            } else {
                                return int1 / int2;
                            }
                        default :
                            throw new InvalidSyntaxException();
                    }
                } else {
                    throw new NegativeOperandException();
                }
            } catch (NumberFormatException e) {
                throw new InvalidSyntaxException();
            }
        }
    }
}
