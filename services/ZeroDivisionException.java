package services;

public class ZeroDivisionException extends Exception {
    public ZeroDivisionException(){
        super("Division par zéro impossible");
    }
}
