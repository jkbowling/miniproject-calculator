package services;

public class NegativeResultException extends Exception {
    public NegativeResultException(){
        super("Le résultat est négatif");
    }
}
