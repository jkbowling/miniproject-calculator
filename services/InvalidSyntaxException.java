package services;

public class InvalidSyntaxException extends Exception {
    public InvalidSyntaxException(){
        super("La syntaxe du calcul est invalide");
    }
}
